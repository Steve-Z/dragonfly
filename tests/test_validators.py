from datetime import date, timedelta

import pytest


@pytest.mark.parametrize(
    "input, output",
    [
        (
            {"origin": "MSPZ", "destination": "NYC"},
            b"That does not appear to be a valid city code",
        ),
        (
            {"origin": "MSP", "destination": "NYCZ"},
            b"That does not appear to be a valid city code",
        ),
        (
            {"origin": "JFK", "destination": "MSP"},
            b"That does not appear to be a valid city code",
        ),
        (
            {"origin": "MSP", "destination": "JFK"},
            b"That does not appear to be a valid city code",
        ),
        # Inspiration and extensive endpoints no longer take currency parameter
        # (
        #     {'origin': "MSP", 'destination': "NYC", 'currency': "USDZ"},
        #     b'That is not in our currency code list'
        # ),
        (
            {"origin": "MSP", "destination": "NYC", "min_duration": 20},
            b"Number must be between 1 and 15",
        ),
        (
            {"origin": "MSP", "destination": "NYC", "min_duration": 0},
            b"Number must be between 1 and 15",
        ),
        # Input validation controlled by html element type.
        # (
        #     {'origin': "MSP", 'destination': "NYC", 'min_duration': "abc"},
        #     b': "abc" [Not a valid integer value]'
        # ),
        (
            {
                "origin": "MSP",
                "destination": "NYC",
                "min_duration": 10,
                "max_duration": 7,
            },
            b"Maximum trip length cannot be less than minimum",
        ),
        (
            {
                "origin": "MSP",
                "destination": "NYC",
                "departure_date1": date.today() - timedelta(days=1),
            },
            b"Dates cannot be prior to today or after 360 days",
        ),
        (
            {
                "origin": "MSP",
                "destination": "NYC",
                "departure_date1": date.today() + timedelta(days=361),
            },
            b"Dates cannot be prior to today or after 360 days",
        ),
        (
            {
                "origin": "MSP",
                "destination": "NYC",
                "departure_date1": date.today() + timedelta(days=10),
                "departure_date2": date.today() + timedelta(days=7),
            },
            b"Latest departure date cannot be earlier than earliest",
        ),
        # (
        #     {'origin': "MSP", 'destination': "NYC", 'max_price': "abc"},
        #     b': "abc" [Not a valid integer value]'
        # ),
        (
            {"origin": "MSP", "destination": "NYC", "max_price": 10},
            b'"10" [Number must be at least 100.]',
        ),
        # (
        #     {
        #         'origin': "MSP",
        #         'destination': "NYC",
        #         'one_way': True,
        #         'aggregation': "DURATION"
        #     },
        #     b'DURATION cannot be selected for one-way searches'
        # )
    ],
)
def test_extensive_validators(client, extensive_url, input, output):
    response = client.post(extensive_url, data=input)
    assert response.status_code == 200
    assert output in response.data
    # Error count is 1 higher because test_client generates error
    # 'csrf_token': ['The CSRF token is missing.']
    assert b'<div class="hidden">2</div>' in response.data


@pytest.mark.parametrize(
    "input, output",
    [
        (
            {
                "origin": "MSP",
                "destination": "NYC",
                "currency": "USD",
                "departureDate": date.today() - timedelta(days=1),
                "returnDate": date.today() + timedelta(days=10),
            },
            b"Dates cannot be prior to today or after 360 days",
        ),
        (
            {
                "origin": "MSP",
                "destination": "NYC",
                "currency": "USD",
                "departureDate": date.today() + timedelta(days=60),
                "returnDate": date.today() + timedelta(days=361),
            },
            b"Dates cannot be prior to today or after 360 days",
        ),
        (
            {
                "origin": "MSP",
                "destination": "NYC",
                "currency": "USD",
                "departureDate": date.today() + timedelta(days=60),
                "returnDate": date.today() + timedelta(days=59),
            },
            b"Return date cannot be earlier than departure date",
        ),
        (
            {
                "origin": "MSP",
                "destination": "NYCZ",
                "currency": "USD",
                "departureDate": date.today() + timedelta(days=60),
                "returnDate": date.today() + timedelta(days=67),
            },
            b"That does not appear to be a valid code",
        ),
        # (
        #     {
        #         'origin': "MSP",
        #         'destination': "NYC",
        #         'currency': "USD",
        #         'maxPrice': "abc",
        #         'departureDate': date.today() + timedelta(days=60),
        #         'returnDate': date.today() + timedelta(days=67)
        #     },
        #     b'"abc" [Not a valid integer value]'
        # ),
        (
            {
                "origin": "MSP",
                "destination": "NYC",
                "currency": "USD",
                "maxPrice": 10,
                "departureDate": date.today() + timedelta(days=60),
                "returnDate": date.today() + timedelta(days=67),
            },
            b'"10" [Number must be at least 100.]',
        ),
    ],
)
def test_live_validators(client, live_url, input, output):
    response = client.post(live_url, data=input)
    assert response.status_code == 200
    assert output in response.data
    # Error count is 1 higher because test_client generates error
    # 'csrf_token': ['The CSRF token is missing.']
    assert b'<div class="hidden">2</div>' in response.data
