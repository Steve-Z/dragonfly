def test_wo_results(mock_results, client, extensive_url):
    with client as c:
        c.get(extensive_url)
        response = c.get("/results/wide_open")
        assert b"Matching flights from" in response.data


def test_live_results(mock_live_results, client, live_url):
    with client as c:
        c.get(live_url)
        response = c.get("/live_results")
        assert b"Live Search Results" in response.data
