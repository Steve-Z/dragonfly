import json
import os.path
import sys

import pytest

path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
if path not in sys.path:
    sys.path.append(path)

from fly_app import create_app  # noqa
from fly_app.routes import search  # noqa


class MockResponse:
    @staticmethod
    def wide_open_response():
        with open(path + "/tests/static/wo_response.json") as f:
            response = json.load(f)
        return response

    @staticmethod
    def live_response():
        with open(path + "/tests/static/live_response.json") as f:
            response = json.load(f)
        return response


@pytest.fixture
def mock_results(monkeypatch):
    def mock_json(*args, **kwargs):
        return MockResponse.wide_open_response()

    monkeypatch.setattr(search, "flight_search", mock_json)


@pytest.fixture
def mock_live_results(monkeypatch):
    def mock_json(*args, **kwargs):
        return MockResponse.live_response()

    monkeypatch.setattr(search, "flight_search", mock_json)


@pytest.fixture
def client():
    app = create_app()
    app.config["TESTING"] = True

    client = app.test_client()
    ctx = app.app_context()
    ctx.push()

    yield client

    ctx.pop()


@pytest.fixture
def extensive_url():
    url = "/flight_search/wide_open"
    return url


@pytest.fixture
def live_url():
    url = "/live_flight_search"
    return url
