# from config import Config
import logging
import os
from logging.handlers import RotatingFileHandler
from secrets import token_urlsafe

from flask import Flask

project_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
fallback_key = token_urlsafe(32)


def create_app():
    # create and configure the app
    app = Flask(__name__)
    app.config.from_mapping(
        SECRET_KEY=os.getenv("SECRET_KEY", fallback_key),
        SESSION_COOKIE_SAMESITE="None",
        SESSION_COOKIE_SECURE=True,
    )

    if not app.debug and not app.testing:
        logs_dir = os.path.join(project_dir, "logs")
        if not os.path.exists(logs_dir):
            os.mkdir(logs_dir)
        file_handler = RotatingFileHandler(
            os.path.join(logs_dir, "dragonfly.log"), maxBytes=10240, backupCount=10
        )
        file_handler.setFormatter(
            logging.Formatter(
                """
            %(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]
            """
            )
        )
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)
        app.logger.setLevel(logging.INFO)
        app.logger.info("Dragonfly startup")

    # with app.app_context():
    from . import routes

    app.register_blueprint(routes.bp)

    from . import errors

    app.register_blueprint(errors.bp)

    return app
