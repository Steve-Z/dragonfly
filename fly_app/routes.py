from flask import (
    render_template,
    request,
    url_for,
    session,
    redirect,
    json,
    flash,
    Blueprint,
    current_app,
)
from fly_app.forms import FlightSearchForm, LiveFlightSearchForm
from .amadeus import Amadeus, date_span, date_time_formatter
from datetime import date, datetime  # , timedelta
import os.path
from isodate import parse_duration

bp = Blueprint("routes", __name__, static_folder="./static")

search = Amadeus()
code_dir = os.path.join(bp.static_folder, "JSON/optd_codes.json")
with open(code_dir) as f:
    code_dict = json.load(f)
currency_dir = os.path.join(bp.static_folder, "JSON/xe_currency_list.json")
with open(currency_dir) as f:
    currencies = json.load(f)


def full_string(param_type, key):
    if key in [
        "origin",
        "originLocationCode",
        "destination",
        "destinationLocationCode",
    ]:
        code = "C" + session[param_type][key]
        string = code_dict[code]["string"]
        fullString = code[1:] + " " + string
    else:
        code = session[param_type][key]
        string = currencies[code]
        fullString = code + " " + string
    return fullString


@bp.route("/")
@bp.route("/index")
def index():
    return render_template("index.html")


@bp.route("/flight_search/wide_open", methods=["GET", "POST"])
def flight_search():
    if request.args.get("new") != "false":
        session["params"] = {  # Needed to avoid keyError
            "oneWay": False,
            "nonStop": False,
            "viewBy": "DESTINATION",
        }
        session["strings"] = {"origin": "", "destination": ""}
    values = session["params"]
    strings = session["strings"]
    one_way = session["params"]["oneWay"]
    direct = session["params"]["nonStop"]
    search_type = request.args.get("search_type", "inspiration")
    if search_type == "extensive":
        form = FlightSearchForm(aggregation=values["viewBy"])
    else:
        form = FlightSearchForm(aggregation="DESTINATION")
    if form.validate_on_submit():
        session["params"] = {}
        session["strings"] = {}
        session["params"]["origin"] = form.origin.data.split()[0].upper()
        session["strings"]["origin"] = form.origin.data
        session["params"]["oneWay"] = form.one_way.data
        if form.destination.data:
            search_type = "extensive"
            destination_ = form.destination.data
            session["strings"]["destination"] = destination_
            session["params"]["destination"] = destination_.split()[0].upper()
            session["params"]["viewBy"] = form.aggregation.data
        else:
            search_type = "inspiration"
            session["params"]["viewBy"] = "DESTINATION"
        if not form.one_way.data:
            if form.min_duration.data and form.max_duration.data:
                session["params"]["duration"] = (
                    str(form.min_duration.data) + "," + str(form.max_duration.data)
                )
            elif form.min_duration.data:  # and not form.max_duration.data:
                session["params"]["duration"] = str(form.min_duration.data)
            elif form.max_duration.data:  # and not form.min_duration.data:
                session["params"]["duration"] = str(form.max_duration.data)
        if form.departure_date1.data and form.departure_date2.data:
            departure_date1 = date.strftime(form.departure_date1.data, "%Y-%m-%d")
            departure_date2 = date.strftime(form.departure_date2.data, "%Y-%m-%d")
            session["params"]["departureDate"] = departure_date1 + "," + departure_date2
        elif form.departure_date1.data:  # and not form.departure_date2.data:
            session["params"]["departureDate"] = date.strftime(
                form.departure_date1.data, "%Y-%m-%d"
            )
        elif form.departure_date2.data:  # and not form.departure_date1.data:
            session["params"]["departureDate"] = date.strftime(
                form.departure_date2.data, "%Y-%m-%d"
            )
        session["params"]["nonStop"] = form.direct_only.data
        if form.max_price.data:
            session["params"]["maxPrice"] = form.max_price.data
        return redirect(url_for("routes.results", search_type=search_type))
    else:
        if form.errors:  # To avoid problem with disableEnable() of duration
            form.one_way.data = False  # Unchecks 'One-way' field
            error_count = len(form.errors.keys())
        else:
            error_count = 0
        return render_template(
            "search.html",
            form=form,
            code_dict=code_dict,
            search_type=search_type,
            one_way=one_way,
            direct=direct,
            values=values,
            strings=strings,
            error_count=error_count,
        )


@bp.route("/results/wide_open", methods=["GET", "POST"])
def results():
    if not request.referrer and not current_app.testing:
        flash(
            """It looks as if you tried accessing results directly through
            the address bar. Results can only be accessed through this search
            page."""
        )
        return redirect(url_for("routes.flight_search"))
    search_type = request.args.get("search_type")
    if request.args.get("referrer"):
        search_type = "extensive"
        # nonStop and oneWay end up as a strings
        session["params"]["nonStop"] = bool(
            session["params"]["nonStop"] == "true"
        )  # string to boolean
        session["params"]["oneWay"] = bool(
            session["params"]["oneWay"] == "true"
        )  # string to boolean
        # Render full strings when Modify Search selected
        session["strings"] = {}
        session["strings"]["origin"] = full_string("params", "origin")
        session["strings"]["destination"] = full_string("params", "destination")
    params = session["params"]
    # currencies = search.CURRENCIES
    flights = search.flight_search(search_type, **params)
    try:
        results = flights["data"]
        return render_template(
            "results.html",
            title="WideOpen Search Results",
            search_type=search_type,
            flights=flights,
            results=results,
            code_dict=code_dict,
            date_span=date_span,
            currencies=currencies,
            set=set,
            list=list,
        )
    except KeyError:
        current_app.logger.exception("Search Parmeters: " + str(params) + str(flights))
        flash(
            """Sorry, we did not find any matching flights.
            Try modifying your search."""
        )
        return redirect(
            url_for("routes.flight_search", new="false", search_type=search_type)
        )


# New flight-offers API requires booleans as strings, eg, 'false', not False
@bp.route("/live_flight_search", methods=["GET", "POST"])
def live_search():
    def immunize(form_data, integer=0):
        if form_data is None:
            form_data = integer
        return form_data

    if request.args.get("new") != "false":
        session["live_params"] = {  # Needed to avoid keyError
            "currencyCode": "USD",
            "nonStop": "false",
            "adults": 1,
            "children": 0,
            "infants": 0,
            "travelClass": "ECONOMY",
        }
        session["live_strings"] = {
            "origin": "",
            "destination": "",
            "currency": "USD US dollars",
        }
    values = session["live_params"]
    strings = session["live_strings"]
    direct = session["live_params"]["nonStop"]
    form = LiveFlightSearchForm(travelClass=values["travelClass"])
    airlines_dir = os.path.join(current_app.static_folder, "JSON/optd_airlines.json")
    with open(airlines_dir) as f:
        airlines_dict = json.load(f)
    if form.validate_on_submit():
        session["live_params"] = {}
        session["live_strings"] = {}
        origin_code = form.origin.data.split()[0].upper()
        session["live_params"]["originLocationCode"] = origin_code
        session["live_strings"]["origin"] = form.origin.data
        destination_code = form.destination.data.split()[0].upper()
        session["live_params"]["destinationLocationCode"] = destination_code
        session["live_strings"]["destination"] = form.destination.data
        currency_code = form.currency.data.split()[0].upper()
        session["live_params"]["currencyCode"] = currency_code
        session["live_strings"]["currency"] = form.currency.data
        session["live_params"]["nonStop"] = str(form.nonStop.data).lower()
        session["live_params"]["departureDate"] = date.strftime(
            form.departureDate.data, "%Y-%m-%d"
        )
        if form.returnDate.data:
            session["live_params"]["returnDate"] = date.strftime(
                form.returnDate.data, "%Y-%m-%d"
            )
        session["live_params"]["adults"] = immunize(form.adults.data, integer=1)
        session["live_params"]["children"] = immunize(form.children.data)
        session["live_params"]["infants"] = immunize(form.infants.data)
        session["live_params"]["travelClass"] = form.travelClass.data

        # New flight-offers API does have arrivalBy/returnBy GET parmaters
        # if form.arrivalBy.data:
        #     arrive_dt = datetime.combine(
        #         form.departureDate.data, form.arrivalBy.data.time())
        #     if form.departurePlusOne.data:
        #         arrive_dt = arrive_dt + timedelta(days=1)
        #     session['live_params']['arrivalBy'] = datetime.strftime(
        #         arrive_dt, '%Y-%m-%dT%H:%M')
        # if form.returnBy.data:
        #     return_dt = datetime.combine(
        #         form.returnDate.data, form.returnBy.data.time())
        #     if form.returnPlusOne.data:
        #         return_dt = return_dt + timedelta(days=1)
        #     session['live_params']['returnBy'] = datetime.strftime(
        #         return_dt, '%Y-%m-%dT%H:%M')

        if form.includeAirlines.data:
            session["live_params"]["includedAirlineCodes"] = form.includeAirlines.data[
                :-1
            ]
        if form.excludeAirlines.data:
            session["live_params"]["excludedAirlineCodes"] = form.excludeAirlines.data[
                :-1
            ]
        if form.maxPrice.data:
            session["live_params"]["maxPrice"] = form.maxPrice.data
        if form.showMax.data:
            session["live_params"]["max"] = form.showMax.data
        return redirect(url_for("routes.live_results"))

    else:
        if form.errors:
            error_count = len(form.errors.keys())
        else:
            error_count = 0
        return render_template(
            "live_search.html",
            # title='Live Flight Search',
            form=form,
            code_dict=code_dict,
            currencies=currencies,
            # search.CURRENCIES,
            direct=direct,
            values=values,
            airlines_dict=airlines_dict,
            strings=strings,
            error_count=error_count,
        )


@bp.route("/live_results", methods=["GET", "POST"])
def live_results():
    if not request.referrer and not current_app.testing:
        flash(
            """It looks as if you tried accessing results directly through the
            address bar. Live Search Results can only be accessed through this
            search page or Inspiration/Extensive results."""
        )
        return redirect(url_for("routes.live_search"))
    if request.args.get("referrer"):
        update = list(session["live_params"].items()) + [
            ("children", 0),
            ("infants", 0),
            ("travelClass", "ECONOMY"),
        ]
        session["live_params"] = dict(update)
        session["live_strings"] = {}
        session["live_strings"]["origin"] = full_string(
            "live_params", "originLocationCode"
        )
        session["live_strings"]["destination"] = full_string(
            "live_params", "destinationLocationCode"
        )
        session["live_strings"]["currency"] = full_string("live_params", "currencyCode")
        result_type = request.args.get("referrer")
    else:
        result_type = ""
    params = session["live_params"]
    flights = search.flight_search("live", **params)
    try:
        results = flights["data"]
        if not results:
            raise KeyError
        return render_template(
            "live_results.html",
            title="Live Search Results",
            flights=flights,
            results=results,
            date_time_formatter=date_time_formatter,
            date_span=date_span,
            strftime=datetime.strftime,
            str=str,
            len=len,
            parse_duration=parse_duration,
            result_type=result_type,
            code_dict=code_dict,
        )
    except KeyError:
        current_app.logger.exception("Search Parmeters: " + str(params) + str(flights))
        flash(
            """Sorry, we did not find any matching flights.
            Try modifying your search."""
        )
        return redirect(url_for("routes.live_search", new="false"))


@bp.route("/parse_redirect")
def parse_redirect():
    link = request.args.get("link")
    referrer = request.args.get("referrer")
    params_list = link.split("?")[1].split("&")
    if "flight-offers" in link:
        session["live_params"] = {}
        #  Add currency key in case it is not in params_list
        session["live_params"]["currency"] = ""
        for i in params_list:
            key = i.split("=")[0]
            session["live_params"][key] = i.split("=")[1]
        # Delete 'bad' currency key and replace with currencyCode
        del session["live_params"]["currency"]
        session["live_params"]["currencyCode"] = request.args.get("currency")
        return redirect(
            url_for(
                "routes.live_results",
                referrer=referrer,
            )
        )
    elif "flight-dates" in link:
        session["params"] = {}
        for i in params_list:
            session["params"][i.split("=")[0]] = i.split("=")[1]
        return redirect(url_for("routes.results", referrer=referrer))
