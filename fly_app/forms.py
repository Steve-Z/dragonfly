# from fly_app import app
import os
from datetime import date, timedelta

from flask import current_app, json
from flask_wtf import FlaskForm
from wtforms import (  # , DateTimeField
    BooleanField,
    DateField,
    IntegerField,
    RadioField,
    SelectField,
    StringField,
    SubmitField,
)
from wtforms.validators import InputRequired, NumberRange, Optional, ValidationError

from .amadeus import Amadeus


def date_in_range(form, field):
    today = date.today()
    max_date = today + timedelta(days=360)
    if field.data < today or field.data > max_date:
        raise ValidationError(
            """Dates cannot be prior to today or after 360 days
                from now"""
        )


def code_check(form, field):
    if field.data is (None or ""):
        pass
    else:
        code_dir = os.path.join(current_app.static_folder, "JSON/optd_codes.json")
        with open(code_dir) as f:
            code_dict = json.load(f)
        if "Live" not in str(form):
            if not ("C" + field.data.split()[0].upper() in code_dict.keys()):
                raise ValidationError("That does not appear to be a valid city code")
        else:
            if not (
                "C" + field.data.split()[0].upper() in code_dict.keys()
                or "A" + field.data.split()[0].upper() in code_dict.keys()
            ):
                raise ValidationError("That does not appear to be a valid code")


def currency_check(form, field):
    # currencies = Amadeus.CURRENCIES
    currency_dir = os.path.join(current_app.static_folder, "JSON/xe_currency_list.json")
    with open(currency_dir) as f:
        currencies = json.load(f)
    if not field.data.split()[0].upper() in currencies.keys():
        raise ValidationError("That is not in our currency code list")


def compare(compare_field, message=None):
    def _compare(form, field):
        if not form._fields[compare_field].data:
            pass
        elif field.data < form._fields[compare_field].data:
            raise ValidationError(message)

    return _compare


class FlightSearchForm(FlaskForm):
    origin = StringField("Origin", validators=[InputRequired(), code_check])
    destination = StringField("Destination", validators=[code_check])
    currency = StringField("Currency", validators=[Optional(), currency_check])
    one_way = BooleanField("One-Way")
    min_duration = IntegerField(
        "Shortest Trip", validators=[Optional(), NumberRange(min=1, max=15)]
    )
    max_duration = IntegerField(
        "Longest Trip",
        validators=[
            Optional(),
            NumberRange(min=1, max=15),
            compare(
                "min_duration",
                message="Maximum trip length cannot be less than minimum",
            ),
        ],
    )
    departure_date1 = DateField("Earliest", validators=[Optional(), date_in_range])
    departure_date2 = DateField(
        "Latest",
        validators=[
            Optional(),
            date_in_range,
            compare(
                "departure_date1",
                message="Latest departure date cannot be earlier than earliest",
            ),
        ],
    )
    direct_only = BooleanField("Direct Flights Only")
    max_price = IntegerField(
        "Maximum Price", validators=[NumberRange(min=100), Optional()]
    )
    aggregation = RadioField(choices=Amadeus.VIEW_BY)
    submit = SubmitField("Search")

    # So search_type can be called by 'form = FlightSearchForm(search_type)'
    # def __init__(self, search_type, *args, **kwargs):
    #     super(FlightSearchForm, self).__init__(*args, **kwargs)
    #     self.search_type = search_type

    # def validate_aggregation(self, aggregation):
    #     if self.one_way.data and aggregation.data == 'DURATION':
    #         raise ValidationError(
    #             'DURATION cannot be selected for one-way searches'
    #             )


class LiveFlightSearchForm(FlaskForm):
    origin = StringField("Origin", validators=[InputRequired(), code_check])
    destination = StringField("Destination", validators=[InputRequired(), code_check])
    currency = StringField("Currency", validators=[InputRequired(), currency_check])
    departureDate = DateField("Departure", validators=[InputRequired(), date_in_range])
    returnDate = DateField(
        "Return",
        validators=[
            Optional(),
            date_in_range,
            compare(
                "departureDate",
                message="Return date cannot be earlier than departure date",
            ),
        ],
    )
    # arrivalBy = DateTimeField(
    #     'Arrive by',
    #     format='%I:%M %p',
    #     validators=[Optional()]
    #     )
    # departurePlusOne = BooleanField('+1 day')
    # returnBy = DateTimeField(
    #     'Return by',
    #     format='%I:%M %p',
    #     validators=[Optional()]
    #     )
    # returnPlusOne = BooleanField('+1 day')
    nonStop = BooleanField("Direct Flights Only")
    adults = IntegerField("Adults (12-64)", validators=[Optional(), NumberRange(min=0)])
    children = IntegerField(
        "Children (2-11)", validators=[Optional(), NumberRange(min=0)]
    )
    infants = IntegerField(
        "Infants (under 2, on lap)", validators=[Optional(), NumberRange(min=0)]
    )
    # seniors = IntegerField(
    #     'Seniors (65+)',
    #     validators=[Optional(), NumberRange(min=0)]
    #     )
    travelClass = SelectField(
        "Travel Class",
        choices=[
            ("ECONOMY", "Economy"),
            ("PREMIUM_ECONOMY", "Premium Economy"),
            ("BUSINESS", "Business Class"),
            ("FIRST", "First Class"),
        ],
        validators=[Optional()],
    )
    includeAirlines = StringField("Search only these airlines", validators=[Optional()])
    excludeAirlines = StringField("Exclude these airlines", validators=[Optional()])
    maxPrice = IntegerField(
        "Maximum Price", validators=[Optional(), NumberRange(min=100)]
    )
    showMax = IntegerField(
        "Limit number of results", validators=[NumberRange(min=1, max=250), Optional()]
    )
    submit = SubmitField("Search")
