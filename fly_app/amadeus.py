import json
import os
import time
from datetime import datetime  # , date, timedelta

import requests


class Amadeus:
    CLIENT_ID = os.environ["AMADEUS_CLIENT_ID"]
    CLIENT_SECRET = os.environ["AMADEUS_CLIENT_SECRET"]
    BASE_URL = "https://api.amadeus.com/"
    # Test env: 'https://test.api.amadeus.com/'
    """ CURRENCIES = {
        "EUR": ["Euros", "&euro;"],
        "COP": ["Columbian pesos", "&#36;"],
        "BRL": ["Brazilian reals", "&#36;"],
        "ARS": ["Argentine pesos", "&#36;"],
        "AUD": ["Australian dollars", "&#36;"],
        "GBP": ["British pounds", "&pound;"],
        "CLP": ["Chilean pesos", "&#36;"],
        "USD": ["US dollars", "&#36;"],
        "CAD": ["Canadian dollars", "&#36;"],
        "RUB": ["Russian rubles", ""],
        "JPY": ["Japanese yen", "&yen;"],
        "CNY": ["Chinese renminbi", "&yen;"],
        "HKD": ["Hong Kong dollars", "&#36;"],
        "INR": ["Indian rupees", "&#8377;"],
        "THB": ["Thai baht", "&#3647;"],
        "MXN": ["Mexican pesos", "&#36;"],
        "SGD": ["Singapore dollars", "&#36;"],
        "AED": ["UAE dirham", ""],
        "CZK": ["Czech koruna", ""],
        "KRW": ["Korean Won", "&#8361;"]
        } """
    # SEARCH_TYPES = ['inspiration', 'extensive']
    VIEW_BY = [
        ("DURATION", "Every combination of date & trip duration"),
        ("DATE", "Each date in departure date range"),
        ("WEEK", "Each week in departure date range"),
        ("DESTINATION", "Just 1 result per destination city")  # ,
        # ('COUNTRY', 'Just 1 result per destination country')
    ]
    PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    CACHE_PATH = os.path.join(PROJECT_DIR, ".cache")
    TIMEOUT = (3.05, 27)

    # To start over will need dummy '.cache' containing only {"expires_in": 0}
    def get_token(self):
        auth_url = self.BASE_URL + "v1/security/oauth2/token"
        client_id = self.CLIENT_ID
        client_secret = self.CLIENT_SECRET
        cache_path = self.CACHE_PATH
        r = requests.post(
            auth_url,
            timeout=self.TIMEOUT,
            data={
                "grant_type": "client_credentials",
                "client_id": client_id,
                "client_secret": client_secret,
            },
        )
        with open(cache_path, "w") as f:
            f.write(r.text)
        return r.json()

    def check_token(self):
        cache_path = self.CACHE_PATH
        with open(cache_path) as f:
            d = json.load(f)
        age = time.time() - os.stat(cache_path).st_mtime
        # Edited to allow for case when faulty json is returned.
        # Consider changing to try/except KeyError
        if "expires_in" not in d.keys() or age > (d["expires_in"] - 30):
            return dict(
                token_type=self.get_token()["token_type"],
                token=self.get_token()["access_token"],
            )
        else:
            return dict(token_type=d["token_type"], token=d["access_token"])

    # def code_lookup(self, code):
    # check_token = self.check_token
    # url = self.BASE_URL + 'reference-data/locations/' + code
    # token_type = check_token()['token_type']
    # token = check_token()['token']
    # r = requests.get(
    #     url,
    #     timeout=self.TIMEOUT,
    #     headers={"Authorization": token_type + " " + token}
    #     )
    # return r.json()

    def flight_search(self, search_type, **params):
        if search_type == "inspiration":
            url = self.BASE_URL + "v1/shopping/flight-destinations"
        elif search_type == "extensive":
            url = self.BASE_URL + "v1/shopping/flight-dates"
        elif search_type == "live":
            url = self.BASE_URL + "v2/shopping/flight-offers"
        check_token = self.check_token
        token_type = check_token()["token_type"]
        token = check_token()["token"]
        r = requests.get(
            url,
            params=params,
            timeout=self.TIMEOUT,
            headers={"Authorization": token_type + " " + token},
        )
        return r.json()


def date_span(date1, date2):
    date1 = date1.split("T")[0]
    date2 = date2.split("T")[0]
    d1 = datetime.strptime(date1, "%Y-%m-%d")
    d2 = datetime.strptime(date2, "%Y-%m-%d")
    diff = d2 - d1
    return diff.days


# def departure_date_in_range(departure_date):
# today = date.today()
# max_date = today + timedelta(days=360)
# return (departure_date.data >= today and departure_date.data <= max_date)


# def date_time_formatter(dtz_string):
# if dtz_string.endswith('Z'):
#     d = dtz_string[:-1] + '+00:00'
# else:
#     d = dtz_string
# s = ''.join(d.rsplit(':', 1))
# return datetime.strptime(s, '%Y-%m-%dT%H:%M:%S%z')


def date_time_formatter(dt_string):
    return datetime.strptime(dt_string, "%Y-%m-%dT%H:%M:%S")
